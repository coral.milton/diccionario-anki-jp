#SCRIPT PARA GENERAR TEXTO A VOZ JAPONES

import gtts
import pandas as pd
import numpy as np
df = pd.read_csv('Gakushuu Kanji lista actualizable.csv', header=None,sep="\t")
df=np.array(df)

for i in range(df.shape[0]):
    tts = gtts.gTTS(str(df[i,1]), lang="ja")
    jls_extract_var = tts
    jls_extract_var.save("/home/milton/DISCOA/info disco 1/script anki/diccionario-anki-jp/audios_temp/"+df[i,0]+".mp3")

