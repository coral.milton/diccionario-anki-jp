# Anki JP Dictionary

## Description

This project includes two scripts designed to facilitate the study of Japanese using Anki:

1. **`anki.ipynb`**: This script generates an Excel file with a list of words and their translations.
2. **`anki.py`**: This script obtains audio pronunciations for the words.

## Instructions for Use

1. **Upload the File**:
   - Upload the `anki.ipynb` file to a Google Colab environment.

2. **Install Libraries**:
   - Install the libraries mentioned in the script in the first cell.

3. **Prepare Data**:
   - In the third cell of the notebook, input the list of words horizontally as shown in the variable `text`, maintaining space or tabulation.

4. **Run the Script**:
   - Execute the script and wait for the final file to be generated.

5. **Download Excel File**:
   - Download the Excel file containing the list of words and their translations.

6. **Run the Local Script**:
   - Run the `anki.py` file locally. Ensure that Python and the necessary libraries are installed, as listed at the beginning of the script.

7. **Place Audio Files**:
   - Once the audio files are generated, place them in the Anki audio folder.

8. **Prepare Excel File for Anki**:
   - Open the Excel file and in the third column, use the formula `'=B:B&"  "&C:C&" [sound:"&A1&".mp3]"'`.

9. **Apply the Formula**:
   - Drag the formula down to apply it to all rows.

10. **Convert to CSV**:
    - Convert the Excel file to CSV, ensuring it is saved with TAB spacing.

11. **Import to Anki**:
    - Import the CSV file into Anki, making sure to change the file extension from `.csv` to `.txt`.

12. **Customize**:
    - Adjust words, corrections, or meanings as desired.

## Recommendations

- **Mobile Use**:
  - Try AnkiDroid to use the whiteboard mode for writing the displayed words in hiragana or kanji.

- **Recommended Plugin for PC**:
  - Consider using the plugin available [here](https://ankiweb.net/shared/info/1631622775).

- **Recommended Books**:
  - Although not included in the previous decks, I recommend the book **オールカラー 学習漢字新辞典 (New Kanji Learning Dictionary - Softcover)**.

- **Future Projects**:
  - I am considering developing decks for advanced levels using the **新漢語林 第二版 (New Outline of Chinese-Origin Words - Second Edition)**. Updates may follow; for now, only the 6th-grade level is in development. I hope this material is helpful for everyone. みんな一緒に頑張ろ！

